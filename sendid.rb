#-*-*-*-*-*-*-*-*-*-*-*-*
#  todo
#-*-*-*-*-*-**-*-*-*-*-*-

#  review ipgen() fix it '41'.xxx.xxx.xxx

#-*-*-*--*-*-*-*-*-*-*-*
#  dependency
#-*-*-*-*-**-*-*-*-*-*-*
require 'socket'
require 'timeout'
require 'net/telnet'
require './lib_trollop.rb'
require './func.rb'

#-*-*-*-*-*-*-*-*-*-*-*-*
#   agruments
#-*-*-*-*-*-*-*-*-*-*-*-*

args = Trollop::options do
	opt :ip_addrs , "IP address singel or a range", :type => :string
	opt :verbose  , "Activate the verbose mode"
	opt :timeout  , "Set time out", :default => 1
	opt :threads  , "Set the number of threads", :default =>10
	opt :export	  , "Export the results to file", :type => :string
end

Trollop::die :ip_addrs,"Argument missing" if args[:ip_addrs] == nil
Trollop::die :timeout, "Must be non-negative" if args[:timeout] <= 0
Trollop::die :threads, "Must be non-negative" if args[:threads] <= 0
Trollop::die :export , "Argument missing" if args[:export] == nil

#-*-*-*-*-*-*-*-*-*-*-*-*
#   Globals
#-*-*-*-*-*-*-*-*-*-*-*-*

$alive_hosts = Array.new
$verbose = args[:verbose]
$timeout = args[:timeout]
$threads_num = args[:threads]

#-*-*-*-*-*-*-*-*-*-*-*-*
#   Locals
#-*-*-*-*-*-*-*-*-*-*-*-*
ip_in = args[:ip_addrs]

#-*-*-*-*-*-*-*-*-*-*-*
#  working aera 
#-*-*-*-*-*-*-*-*-*-*-*

begin

	#-------> the scanner

	time_start=Time.now
	hosts=ipgen(ip_in) #generat ips from inputed ip range
	time_end=Time.now
	puts "[*] #{hosts.count} Ip adress generated in #{time_end - time_start} seconds" if $verbose && hosts.count > 1

	_ = "[*] starting the scan of #{hosts.count} hosts"
	_ = "[*] starting the scan of one host" if hosts.count == 1
	puts _

	time_start=Time.now

	threads=[]
	hosts.each do |host|
		# start threading
		if(Thread.list.count % $threads_num != 0) 
	    	mythread = Thread.new do
	    		scan(host)  
	    	end
	    threads << mythread
		else
	    	# Wait for open threads to finish executing 
	    	# before starting new one
	    	threads.each do |thread|
	      		thread.join
	    	end
	    # Start a new thread again
	    	mythread = Thread.new do
	    		scan(host)
	    	end
	    threads << mythread
	  	end
	end

	# Wait for threads to finish executing before exiting the program
	threads.each do |thread|
		thread.join
	end	

	time_end=Time.now

	_ =  "[*] found #{$alive_hosts.count} alive hosts in #{time_end - time_start} seconds"
	_ =  "[*] found one alive host in #{time_end - time_start} seconds" if $alive_hosts.count == 1
	puts _
	
	puts "[*] alive hosts:" if $verbose  && $alive_hosts.count != 0
	#/!\ fixit : do a foreach loop insted of printing the raw array 
	#puts "#{$alive_hosts}" if $verbose && $alive_hosts.count != 0 

	#-----------> the cracker
	puts "[*] start checking for hosts with weak password"

	time_start = Time.now

	$alive_hosts.each do |host|
		host_crd = checkpwd(host)
		if host_crd != nil
			output= "[*] the host:#{host}\n[+] #{host_crd["usr"]}\n[+] #{host_crd["pwd"]}\n"
			print output
			export(args[:export],output)
		else
			#puts "[D] empty host_crd" # for debugging 
		end 
	end
	
	time_end = Time.now

	puts "finished checking host in #{time_end - time_start} seconds"

rescue (Interrupt)
	puts "\n[*] Interrupted !!"
end